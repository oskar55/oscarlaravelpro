@extends('layouts.app')

@section('content')
<h1>Mensaje id: {{ $message->id }} </h1>
<img src="{{ $message->image }} " alt="">
<p class="card-text">
	{{ $message->content }}
	<small class="text-muted"> {{ $message->created_at }} </small>
</p>
@endsection
