@extends('layouts.app')



@section('title')
LARAVEL - Oscar PRO 
@endsection


@section('content')


<div class="row">
    @forelse ($messages as $message)
        <div class="col-6">
            <img class="img-thumbnail" alt="" src="{{ $message->image }} ">
            <p class="card-text"> 
                {{ $message->content }} 
                <a href="/message/">Leer mas </a>
            </p>
        </div>
    @empty
        <p>No hay mensajes Destacados</p>
    @endforelse
</div>
@endsection