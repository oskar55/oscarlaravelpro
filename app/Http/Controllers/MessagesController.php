<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;

class MessagesController extends Controller
{
    
    public function show(Message $message){
    	//buscar el mensaje por id
    	//$message = Message::find($id);

    	//una vista de un message
    	return view('messages.show',[
    		'message'=> $message,
    	]);
    }


}
