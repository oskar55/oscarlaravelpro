<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// importar el modelo Message
use App\Message;


class PagesController extends Controller
{
    //funciones para las rutas

    public function home(){

    	//se puedde hacer un array o cualquier tipo de codigo en las funciones (metodos) del controlador par aluego transportar su contenido o resultados a travez del render de las rutas o visatas, en este ejemplo se le encia el array $messages a la vista wellcome, esto se envia siempre por el segundo parametro de la vista.
    	$messages = Message::all(); 

    	//enviando el array messages a la vista welcome por medio del segundo parametro de view('vista','pasar_valores')
    	return view('welcome', [
    		'messages' => $messages
    	]);
    }


    public function aboutUs(){
    	return view('/about');
    } 

    public function portfolio(){
    	return ("este sera mi portafolio");
    }
}
