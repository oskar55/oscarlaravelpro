<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//rutas manejadas desde controladores
Route::get('/', 'PagesController@home');

Route::get('/messages/{message}', 'MessagesController@show');


Route::get('/acerca', 'PagesController@aboutUs');


Route::get('/porfolio', 'PagesController@portfolio');